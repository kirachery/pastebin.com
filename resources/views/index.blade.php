<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body style="padding: 20px;">
    <div class="wrapper" style="background-color: #7aa6da;width: 960px; margin: auto">
        <form action="/" method="POST">
            @csrf
            <textarea rows="14" name="text"></textarea>
            <select name="time" id="">
                @foreach ($pars['times']->all() as $time)
                    <option value="{{$time['id']}}">{{$time['name']}}</option>
                @endforeach
            </select>
            <select name="access" id="">
                @foreach ($pars['accesses']->all() as $access)
                    <option value="{{$access['id']}}">{{$access['name']}}</option>
                @endforeach
            </select>
            <input type="submit">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </form>

        <div class="add_block">
            <?foreach ($pars["last_blocks"] as $block) {
                echo "<a href='/".$block->hash."'>"."/".$block->hash."</a><br>";
            }?>
        </div>
    </div>


    <style>
        div {box-sizing: border-box}
        form {
            padding: 40px;
            line-height: 2;
        }
        select {
            width: 25%;
        }
        input[type="submit"] {
            width: 25%;
            float:right;
            line-height: 2;
            cursor: pointer;
        }
        textarea {
            width: 100%;
            padding: 10px;
        }
        .add_block {
            border: 1px solid blue;
            border-radius:10px;
            //width: 25%;
            margin-top: 10px;
            padding: 10px;
            line-height: 1.5;
            float: right;
        }
    </style>
</body>
</html>