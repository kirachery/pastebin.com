<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BlocksController@index');
Route::get('/{hash}', 'BlocksController@show');
Route::post('/','BlocksController@save');
/*Route::get('/{hash}', function ($hash) {
    $last_blocks = App\Block::tenlast();
    return view('block',compact("last_blocks"));

});
