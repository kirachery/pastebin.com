<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Block extends Model
{
    public static function tenlast(){
        //return DB::table('blocks')->limit(10)->latest()->get();
        return DB::select(DB::raw("SELECT * FROM `blocks` INNER JOIN times ON times.id = blocks.time WHERE NOW() - blocks.created_at < times.sec ORDER BY blocks.created_at DESC LIMIT 10"));
    }

    public static function find($hash){
        //return DB::table('blocks')->where('hash', '=', $hash)->get();
        return static::where('hash',$hash)->get()->first();
    }

    public static function insert($data){
        DB::table('blocks')->insert($data);
    }
}
