<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
</head>
<body style="padding: 60px;">
    <div class="wrapper" style="padding: 40px;background-color: #7aa6da;width: 960px; margin: auto">
        <h1 style="text-align: center;">{{ $block->name }}</h1>
        <div style="padding: 20px; min-height: 400px; width: 100%; background-color: #FFF">
            <i> {{ $block->text }}</i></div>
        <a href="/">Назад</a>
    </div>

    <style>
        div {box-sizing: border-box}
    </style>
</body>
</html>