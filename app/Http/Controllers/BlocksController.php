<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Block;
use App\Time;
use App\Access;


class BlocksController extends Controller
{
    public function index() {
        $pars = array();
        $pars["last_blocks"] = Block::tenlast();
        $pars["times"] = Time::all();
        $pars["accesses"] = Access::all();

        return view('index',compact("pars"));
    }

    public function show($hash) {
        $block = Block::find($hash);
        return view('show',compact('block'));
    }

    public function save() {
        $data_obj = Request::capture();
        $data = array(
            "text" => $data_obj["text"],
            "access"=>$data_obj["access"],
            "time" => $data_obj["time"],
            "hash" => md5(date('h-i-s, j-m-y').$data_obj["text"].$data_obj["access"].$data_obj["time"]));
        Block::insert($data);
        return redirect("/");

    }
}